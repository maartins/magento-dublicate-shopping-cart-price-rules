<?php
class Custom_Salesruledublicator_Block_Adminhtml_Promo_Quote_Grid extends Mage_Adminhtml_Block_Promo_Quote_Grid
{
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('salesrule/rule')
            ->getResourceCollection()
        ;
                $collection->getSelect();
        $this->setCollection($collection);

        @Mage_Adminhtml_Block_Widget_Grid::_prepareCollection();
        return $this;
    }
    
    protected function _prepareColumns()
    {
        parent::_prepareColumns();
        
        $positionColumn = $this->getColumn('sort_order');
        if ($positionColumn)
            $positionColumn->setWidth('20px');        
        
        $this->addColumnAfter('simple_action', array(
            'header'    => Mage::helper('salesrule')->__('Action'),
            'width'     => '120px',
            'type'      => 'options',
            'options'   => $this->getDiscountTypes(),
            'index'     => 'simple_action',
        ), 'coupon_code');
        
        $this->addColumnAfter('discount_amount', array(
            'header'    => Mage::helper('sales')->__('Discount'), //its correct
            'align'     => 'right',
            'index'     => 'discount_amount',
            'getter'     => array($this, 'formatDiscount'),
        ), 'simple_action');
        
        $this->addColumnAfter('stop_rules_processing', array(
            'header'    => $this->__('Stop'),
            'index'     => 'stop_rules_processing',
            'type'      => 'options',
            'options'   => array(
                1 => Mage::helper('salesrule')->__('Yes'),
                0 => Mage::helper('salesrule')->__('No'),
            ),
        ), 'to_date');  
        
        $this->addColumn('action',array(
            'header'    => Mage::helper('catalog')->__('Action'), //its correct
            'width'     => '50px',
            'type'      => 'action',
            'actions'   => array(
                array(
                    'caption' => Mage::helper('catalog')->__('Duplicate'),
                    'url'     => array('base' => 'adminhtml/salesruledublicator_index/duplicate'),
                    'field'   => 'rule_id'
                )
            ),
            'filter'    => false,
            'sortable'  => false,
            'index'     => 'rule_id',
            'is_system' => true,
        ));               
        
        $this->sortColumnsByOrder();
       
        return $this;
        
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('rule_id');
        $this->getMassactionBlock()->setFormFieldName('rules');
        
        $this
            ->addAction('Activate','massEnable')
            ->addAction('De-activate','massDisable')
            ->addAction('Delete', 'massDelete', true)
        ;
            
        return $this; 
    }    

    protected function addAction($label, $urlKey='', $isConfim=false)   
    {
        $this->getMassactionBlock()->addItem(str_replace('/', '_', $urlKey), array(
             'label'    => $this->__($label),
             'url'      => $this->getUrl('adminhtml/salesruledublicator_index/'. $urlKey),
             'confirm'  => $isConfim ? $this->__('Are you sure?') : null,
        )); 
        
        return $this;       
    }

    protected function getDiscountTypes()
    {
        $options = array( // for 1.4.1 compatibility, we can't use constants
            'by_percent'  => Mage::helper('catalog')->__('Price Percent'),
            'by_fixed'    => Mage::helper('catalog')->__('Product Amount'),
            'cart_fixed'  => Mage::helper('catalog')->__('Cart Amount'),
            'buy_x_get_y' => Mage::helper('catalog')->__('Buy X get Y'),
            'ampromo_items' => Mage::helper('catalog')->__('Auto add promo items with products'),
            'ampromo_cart' => Mage::helper('catalog')->__('Auto add promo items for the whole cart'),
            'ampromo_product' => Mage::helper('catalog')->__('Auto add the same product'),
            'ampromo_spent' => Mage::helper('catalog')->__('Auto add promo items for every $X spent'),
        );
                
        return $options;
    }
    
    public function formatDiscount($row)
    {
        return number_format($row->getData('discount_amount'),2);
    }
    
}
