<?php
/**
 * Created by PhpStorm.
 * User: martinssaukums
 * Date: 16/02/16
 * Time: 15:11
 */

class Custom_Salesruledublicator_Adminhtml_Salesruledublicator_IndexController extends Mage_Adminhtml_Controller_Action {

    protected $gridUrl = 'adminhtml/promo_quote/index';

    public function indexAction()
    {
        return $this->_fault($this->__('Please select some action.'));
    }

    public function duplicateAction()
    {
        $id = $this->getRequest()->getParam('rule_id');
        if (!$id) {
            return $this->_fault($this->__('Please select a rule to duplicate.'));
        }

        try {
            $mainRule = Mage::getSingleton('salesrule/rule')->load($id);
            if (!$mainRule->getId()){
                return $this->_fault($this->__('Please select a rule to duplicate.'));
            }

            //just pre-load values
            $mainRule->getStoreLabels();

            // a proper clone function has not been implemented
            // for the rule class, so we need to unlink coupon object manually
            $rule = clone $mainRule;
            $oldCoupon = $rule->acquireCoupon();
            if ($oldCoupon){
                $oldCoupon->setId(0);
            }

            // set default data
            $rule->setCouponType(Mage_SalesRule_Model_Rule::COUPON_TYPE_NO_COUPON);
            $rule->setIsActive(0);

            //create new acually
            $rule->setId(null);
            $rule->save();

            $this->_getSession()->addSuccess(
                $this->__('The rule has been duplicated. Set a new coupon and activate it if needed.')
            );
            return $this->_redirect('adminhtml/promo_quote/edit', array('id' => $rule->getId()));
        }
        catch (Exception $e) {
            return $this->_fault($e->getMessage());
        }

        //unreachable
        return $this;
    }

    public function massDeleteAction()
    {
        $ids = $this->getRequest()->getParam('rules');
        if (!is_array($ids)) {
            return $this->_fault($this->__('Please select rule(s).'));
        }

        $num = 0;
        foreach ($ids as $id) {
            try {
                $rule = Mage::getSingleton('salesrule/rule')->load($id);
                $rule->getPrimaryCoupon()->delete();
                $rule->delete();
                ++$num;
            }
            catch (Exception $e) {
                $this->_fault($e->getMessage(), false);
            }
        }
        return $this->_success($this->__('Total of %d record(s) have been deleted.', $num));
    }

    public function massEnableAction()
    {
        return $this->_modifyStatus(1);
    }

    public function massDisableAction()
    {
        return $this->_modifyStatus(0);
    }

    protected function _modifyStatus($status)
    {
        $ids = $this->getRequest()->getParam('rules');
        if (!is_array($ids)) {
            return $this->_fault($this->__('Please select rule(s).'));
        }
        $num = 0;
        foreach ($ids as $id) {
            try {
                $rule = Mage::getModel('salesrule/rule')->load($id);
                if ($rule->getIsActive() != $status){
                    $rule->setIsActive($status);
                    $rule->save();
                    ++$num;
                }
            }
            catch (Exception $e) {
                $this->_fault($e->getMessage(), false);
            }
        }
        return $this->_success($this->__('Total of %d record(s) have been updated.', $num));
    }

    protected function _fault($message, $redirect=true)
    {
        $this->_getSession()->addError($message);
        if ($redirect)
            $this->_redirect($this->gridUrl);

        return $this;
    }

    protected function _success($message, $redirect=true)
    {
        $this->_getSession()->addSuccess($message);
        if ($redirect)
            $this->_redirect($this->gridUrl);

        return $this;
    }
}